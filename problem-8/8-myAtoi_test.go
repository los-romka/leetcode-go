package problem_8

import (
	"math"
	"testing"
)

func Test_myAtoi(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"Example 1", args{"42"}, 42},
		{"Example 2", args{"   -42"}, -42},
		{"Example 3", args{"4193 with words"}, 4193},
		{"Test 1", args{"words and 987"}, 0},
		{"Test 2", args{"-91283472332"}, math.MinInt32},
		{"Test 3", args{"91283472332"}, math.MaxInt32},
		{"Test 4", args{" "}, 0},
		{"Test 5", args{"9223372036854775808"}, math.MaxInt32},
		{"Test 6", args{"-9223372036854775808"}, math.MinInt32},
		{"Sample 1", args{"0032"}, 32},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := myAtoi(tt.args.s); got != tt.want {
				t.Errorf("myAtoi() = %v, want %v", got, tt.want)
			}
		})
	}
}
