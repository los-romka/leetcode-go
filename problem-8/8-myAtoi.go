package problem_8

import "math"

func myAtoi(s string) int {
	if s == "" {
		return 0
	}

	slen := len(s)

	i := 0

	for ; i < slen && s[i] == 32; i++ { // whitespace
	}

	sign := 1

	if i < slen && (s[i] == 43 || s[i] == 45) { // plus or minus
		sign = 44 - int(s[i])
		i++
	}

	digit := int8(0)
	number := 0

	for ; i < slen; i++ { // digits
		digit = int8(s[i] - 48)

		if digit >= 0 && digit <= 9 {
			number = number*10 + sign*int(digit)
		} else {
			break
		}

		if number > math.MaxInt32 {
			return math.MaxInt32
		}

		if number < math.MinInt32 {
			return math.MinInt32
		}
	}

	return number
}
