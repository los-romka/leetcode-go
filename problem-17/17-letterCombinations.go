package problem_17

var keyboard = []string{"abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"}

func letterCombinations(digits string) []string {
	if digits == "" {
		return []string{}
	}

	result := []string{""}

	for _, d := range digits {
		pos := d - 50

		lenBeforeCopy := len(result)
		for _, r := range result {
			for _, c := range keyboard[pos] {
				result = append(result, r+string(c))
			}
		}
		result = result[lenBeforeCopy:]
	}

	return result
}
