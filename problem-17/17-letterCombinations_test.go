package problem_17

import (
	"reflect"
	"sort"
	"testing"
)

func Test_letterCombinations(t *testing.T) {
	type args struct {
		digits string
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{"Example 1", args{"23"}, []string{"ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"}},
		{"Example 2", args{""}, []string{}},
		{"Example 3", args{"2"}, []string{"a", "b", "c"}},
		{"Sample 1", args{"22"}, []string{"aa", "ab", "ac", "ba", "bb", "bc", "ca", "cb", "cc"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := letterCombinations(tt.args.digits)
			sort.Strings(got)

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("letterCombinations() = %v, want %v", got, tt.want)
			}
		})
	}
}
