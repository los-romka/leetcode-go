package problem_5

import "testing"

func Test_longestPalindrome(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"Example 1", args{"babad"}, "bab"},
		{"Example 2", args{"cbbd"}, "bb"},
		{"Test 1", args{"ac"}, "a"},
		{"Test 2", args{"abb"}, "bb"},
		{"Test 3", args{"babaddtattarrattatddetartrateedredividerb"}, "ddtattarrattatdd"},
		{"Sample 1", args{"a"}, "a"},
		{"Sample 2", args{"aa"}, "aa"},
		{"Sample 3", args{"aba"}, "aba"},
		{"Sample 4", args{"ababa"}, "ababa"},
		{"Sample 5", args{"abcdaa"}, "aa"},
		{"Sample 6", args{"abcdaaa"}, "aaa"},
		{"Sample 7", args{"aabcdaaa"}, "aaa"},
		{"Sample 8", args{"aabcda"}, "aa"},
		{"Sample 9", args{"aababacda"}, "ababa"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := longestPalindrome(tt.args.s); got != tt.want {
				t.Errorf("longestPalindrome() = %v, want %v", got, tt.want)
			}
		})
	}
}
