package problem_5

func longestPalindrome(s string) string {
	if s == "" {
		return ""
	}

	longest := s[0:1]

	last := len(s) - 1

	for pivot := 0; pivot < last; pivot++ {
		l := pivot
		r := pivot + 1

		for r <= last && s[l] == s[r] {
			r++
		}

		r--

		for l >= 0 && r <= last {
			if s[l] != s[r] {
				break
			}

			l--
			r++
		}

		l++

		if len(s[l:r]) > len(longest) {
			longest = s[l:r]
		}
	}

	return longest
}
