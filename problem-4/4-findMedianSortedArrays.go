package problem_4

func findMedianSortedArrays(nums1 []int, nums2 []int) float64 {
	m, n := int16(len(nums1)), int16(len(nums2))
	mn := m + n
	mid := mn / 2

	ml := int16(0)
	nl := int16(0)

	var last = 0
	var prev = 0

	for i := int16(0); i <= mid; i++ {
		prev = last

		if ml < m && (nl == n || nums1[ml] < nums2[nl]) {
			last = nums1[ml]
			ml++
		} else {
			last = nums2[nl]
			nl++
		}
	}

	if mn%2 == 0 {
		return float64(prev+last) / 2
	}

	return float64(last)
}
