package problem_4

import "testing"

func Test_findMedianSortedArrays(t *testing.T) {
	type args struct {
		nums1 []int
		nums2 []int
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{"Example 1", args{[]int{1, 3}, []int{2}}, 2.00000},
		{"Example 2", args{[]int{1, 2}, []int{3, 4}}, 2.50000},
		{"Test 1", args{[]int{100000}, []int{100001}}, 100000.50000},
		{"Sample 1", args{[]int{2}, []int{1, 3}}, 2.00000},
		{"Sample 2", args{[]int{3, 4}, []int{1, 2}}, 2.50000},
		{"Sample 3", args{[]int{1, 2, 3}, []int{1, 2, 3}}, 2.00000},
		{"Sample 4", args{[]int{}, []int{1, 3}}, 2.00000},
		{"Sample 5", args{[]int{1, 3}, []int{}}, 2.00000},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := findMedianSortedArrays(tt.args.nums1, tt.args.nums2); got != tt.want {
				t.Errorf("findMedianSortedArrays() = %v, want %v", got, tt.want)
			}
		})
	}
}
