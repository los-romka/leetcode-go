package problem_12

import "testing"

func Test_intToRoman(t *testing.T) {
	type args struct {
		num int
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"Example 1", args{3}, "III"},
		{"Example 2", args{58}, "LVIII"},
		{"Example 3", args{1994}, "MCMXCIV"},
		{"Sample 1", args{1}, "I"},
		{"Sample 2", args{2}, "II"},
		{"Sample 3", args{4}, "IV"},
		{"Sample 4", args{5}, "V"},
		{"Sample 5", args{6}, "VI"},
		{"Sample 6", args{7}, "VII"},
		{"Sample 7", args{10}, "X"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := intToRoman(tt.args.num); got != tt.want {
				t.Errorf("intToRoman() = %v, want %v", got, tt.want)
			}
		})
	}
}
