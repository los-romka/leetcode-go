package problem_12

import (
	"strings"
)

var letters = "IVXLCDM"

func intToRoman(num int) string {
	result := ""

	pos := 0
	for num > 0 {
		digit := num % 10
		num = num / 10

		if digit == 0 {
		} else if digit <= 3 {
			result = strings.Repeat(string(letters[pos]), digit) + result
		} else if digit == 4 || digit == 9 {
			result = string(letters[pos]) + string(letters[pos+digit/4]) + result
		} else {
			result = string(letters[pos+1]) + strings.Repeat(string(letters[pos]), digit-5) + result
		}

		pos = pos + 2
	}

	return result
}
