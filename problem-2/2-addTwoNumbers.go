package problem_2

type ListNode struct {
	Val  int
	Next *ListNode
}

/**
 * Definition for singly-linked list.
 */
func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
	var node *ListNode = nil
	var head *ListNode = nil
	var remember = 0

	for l1 != nil || l2 != nil || remember > 0 {
		sum := remember

		if l1 != nil {
			sum = sum + l1.Val
			l1 = l1.Next
		}

		if l2 != nil {
			sum = sum + l2.Val
			l2 = l2.Next
		}

		remember = sum / 10
		sum = sum % 10

		if node == nil {
			node = &ListNode{sum, nil}
			head = node
		} else {
			node.Next = &ListNode{sum, nil}
			node = node.Next
		}
	}

	return head
}
