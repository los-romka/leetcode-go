package problem_2

import (
	"reflect"
	"strconv"
	"strings"
	"testing"
)

func makeList(str string) *ListNode {
	digits := strings.Split(str, "")

	var node *ListNode = nil
	var prev *ListNode = nil

	for _, digit := range digits {
		val, _ := strconv.Atoi(digit)

		node = &ListNode{val, prev}
		prev = node
	}

	return node
}

func Test_addTwoNumbers(t *testing.T) {
	type args struct {
		l1 *ListNode
		l2 *ListNode
	}
	tests := []struct {
		name string
		args args
		want *ListNode
	}{
		{"Example 1", args{makeList("342"), makeList("465")}, makeList("807")},
		{"Example 2", args{makeList("0"), makeList("0")}, makeList("0")},
		{"Example 3", args{makeList("9999999"), makeList("9999")}, makeList("10009998")},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := addTwoNumbers(tt.args.l1, tt.args.l2); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("addTwoNumbers() = %v, want %v", got, tt.want)
			}
		})
	}
}
