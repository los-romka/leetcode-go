package problem_3

func lengthOfLongestSubstring(s string) int {
	var visited [127]int16

	var startIdx int16 = 0
	var max int16 = 0

	for idx, char := range s {
		var endIdx = int16(idx)

		c := int8(char)

		lastIdx := visited[int8(char)]

		if lastIdx > startIdx {
			startIdx = lastIdx
		}

		visited[c] = endIdx + 1

		length := endIdx - startIdx + 1

		if length > max {
			max = length
		}
	}

	return int(max)
}
