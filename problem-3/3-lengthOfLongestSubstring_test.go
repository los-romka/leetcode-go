package problem_3

import "testing"

func Test_lengthOfLongestSubstring(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"Example 1", args{s: "abcabcbb"}, 3},
		{"Example 2", args{s: "bbbbb"}, 1},
		{"Example 3", args{s: "pwwkew"}, 3},
		{"Test 1", args{s: "aab"}, 2},
		{"Test 2", args{s: ""}, 0},
		{"Test 3", args{s: "dvdf"}, 3},
		{"Test 4", args{s: " "}, 1},
		{"Sample 1", args{s: "dvavdf"}, 4},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := lengthOfLongestSubstring(tt.args.s); got != tt.want {
				t.Errorf("lengthOfLongestSubstring() = %v, want %v", got, tt.want)
			}
		})
	}
}
