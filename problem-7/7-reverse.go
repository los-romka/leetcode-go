package problem_7

import "math"

func reverse(x int) int {
	if x == 0 {
		return x
	}

	sign := 1

	if x < 0 {
		sign = -1
		x = -x
	}

	r := 0

	for x > 0 {
		r = r * 10
		r = r + (x % 10)
		x = x / 10
	}

	if r > math.MaxInt32 || r < math.MinInt32 {
		return 0
	}

	return sign * r
}
