package problem_7

import (
	"math"
	"testing"
)

func Test_reverse(t *testing.T) {
	type args struct {
		x int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"Example 1", args{123}, 321},
		{"Example 2", args{-123}, -321},
		{"Example 3", args{120}, 21},
		{"Sample 1", args{0}, 0},
		{"Sample 2", args{7}, 7},
		{"Sample 3", args{17}, 71},
		{"Sample 4", args{1}, 1},
		{"Sample 5", args{11}, 11},
		{"Sample 6", args{10}, 1},
		{"Sample 7", args{100}, 1},
		{"Sample 8", args{101}, 101},
		{"Sample 9", args{1010}, 101},
		{"Sample 10", args{math.MaxInt32}, 0},
		{"Sample 11", args{math.MinInt32}, 0},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := reverse(tt.args.x); got != tt.want {
				t.Errorf("reverse() = %v, want %v", got, tt.want)
			}
		})
	}
}
