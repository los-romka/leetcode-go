package problem_13

import "testing"

func Test_romanToInt(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"Example 1", args{"III"}, 3},
		{"Example 2", args{"LVIII"}, 58},
		{"Example 3", args{"MCMXCIV"}, 1994},
		{"Sample 1", args{"I"}, 1},
		{"Sample 2", args{"II"}, 2},
		{"Sample 3", args{"IV"}, 4},
		{"Sample 4", args{"V"}, 5},
		{"Sample 5", args{"VI"}, 6},
		{"Sample 6", args{"VII"}, 7},
		{"Sample 7", args{"X"}, 10},
		{"Sample 8", args{"XVI"}, 16},
		{"Sample 9", args{"XIV"}, 14},
		{"Sample 10", args{"XV"}, 15},
		{"Sample 11", args{"XCV"}, 95},
		{"Sample 12", args{"IX"}, 9},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := romanToInt(tt.args.s); got != tt.want {
				t.Errorf("romanToInt() = %v, want %v", got, tt.want)
			}
		})
	}
}
