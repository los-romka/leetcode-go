package problem_13

func romanToInt(s string) int {
	letters := []uint8{'I', 'V', 'X', 'L', 'C', 'D', 'M'}
	values := []int{1, 5, 10, 50, 100, 500, 1000}

	num := 0
	pos := 0

	for i := len(s) - 1; i >= 0; {
		if letters[pos] == s[i] {
			num += values[pos]

			i--

			minusPos := ((pos - 1) / 2) * 2

			if i >= 0 && letters[pos] != s[i] && s[i] == letters[minusPos] {
				num -= values[minusPos]
				i--
			}
		} else {
			pos++
		}
	}

	return num
}
