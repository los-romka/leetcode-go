package problem_15

import "sort"

func threeSum(nums []int) [][]int {
	sort.Ints(nums)

	result := [][]int{}

	lessZeroIdx := -1
	moreZeroIdx := len(nums)

	zeros := 0
	for k := len(nums) - 1; k >= 0; k-- {
		if nums[k] > 0 {
			moreZeroIdx--
		} else if nums[k] < 0 {
			lessZeroIdx++
		} else {
			zeros++
		}
	}

	if zeros >= 3 {
		result = append(result, []int{0, 0, 0})
	}

	if moreZeroIdx < len(nums) {
		for pivot := 0; pivot <= lessZeroIdx; pivot++ {
			hasZero := zeros > 0

			for nums[pivot] == nums[pivot+1] {
				pivot++
			}

			i := moreZeroIdx
			j := len(nums) - 1

			if hasZero && (nums[pivot] == -nums[j] || nums[pivot] == -nums[i]) {
				result = append(result, []int{nums[pivot], 0, -nums[pivot]})
				hasZero = false
				j--
			}

			for i < j {
				if nums[pivot]+nums[i]+nums[j] > 0 {
					if nums[pivot] == -nums[j] && hasZero {
						result = append(result, []int{nums[pivot], 0, nums[j]})
						hasZero = false
					}

					j--
				} else if nums[pivot]+nums[i]+nums[j] < 0 {
					i++
				} else {
					for nums[j] == nums[j-1] {
						j--
					}

					result = append(result, []int{nums[pivot], nums[i], nums[j]})
					if nums[i] == nums[j] {
						break
					}
					j--
				}
			}
		}
	}

	if lessZeroIdx > 0 {
		for pivot := len(nums) - 1; pivot >= moreZeroIdx; pivot-- {
			for nums[pivot] == nums[pivot-1] {
				pivot--
			}

			i := 0
			j := lessZeroIdx

			for i < j {
				if nums[pivot]+nums[i]+nums[j] > 0 {
					j--
				} else if nums[pivot]+nums[i]+nums[j] < 0 {
					i++
				} else {
					for nums[i] == nums[i+1] {
						i++
					}

					result = append(result, []int{nums[pivot], nums[i], nums[j]})

					if nums[i] == nums[j] {
						break
					}
					i++
				}
			}
		}
	}

	if result == nil {
		return [][]int{}
	}

	return result
}
