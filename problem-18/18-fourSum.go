package problem_18

import "sort"

func dumpSolution(nums []int, target int) [][]int {
	sort.Ints(nums)

	resultMap := make(map[int64][]int, len(nums))

	for a := 0; a < len(nums); a++ {
		for b := a + 1; b < len(nums); b++ {
			for c := b + 1; c < len(nums); c++ {
				for d := c + 1; d < len(nums); d++ {
					if nums[a]+nums[b]+nums[c]+nums[d] == target {
						quadrant := []int{nums[a], nums[b], nums[c], nums[d]}
						sort.Ints(quadrant)

						key := int64(quadrant[0]) + 2*int64(quadrant[1]) + 3*int64(quadrant[2]) + 5*int64(quadrant[3])

						resultMap[key] = quadrant
					}
				}
			}
		}
	}

	result := [][]int{}

	for _, v := range resultMap {
		result = append(result, v)
	}

	if result == nil {
		return [][]int{}
	}

	return result
}

func fourSum(nums []int, target int) [][]int {
	return dumpSolution(nums, target)
}
