package problem_16

import "testing"

func Test_threeSumClosest(t *testing.T) {
	type args struct {
		nums   []int
		target int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"Example 1", args{[]int{-1, 2, 1, -4}, 1}, 2},
		{"Example 2", args{[]int{0, 0, 0}, 1}, 0},
		{"Sample 1", args{[]int{0, 0, 1}, 1}, 1},
		{"Sample 2", args{[]int{-50, 0, 0, 0, 50}, 50}, 50},
		{"Sample 3", args{[]int{-50, 0, 0, 0, 49}, 25}, 49},
		{"Sample 4", args{[]int{1, 1, 1}, 50}, 3},
		{"Sample 5", args{[]int{-20, -1, 0, 1, 20}, 20}, 20},
		{"Sample 6", args{[]int{-20, -1, 0, 1, 20}, 30}, 21},
		{"Sample 7", args{[]int{-20, -1, 0, 1, 20}, -20}, -20},
		{"Sample 8", args{[]int{-20, -1, 0, 1, 20}, -30}, -21},
		{"Sample 9", args{[]int{-20, -1, 0, 1, 20}, 0}, 0},
		{"Sample 10", args{[]int{-20, -1, 1, 1, 25}, 0}, 1},
		{"Sample 11", args{[]int{-25, -1, 1, 1, 20}, 0}, 1},
		{"Sample 12", args{[]int{-25, -1, 1, 2, 20}, -24}, -24},
		{"Sample 13", args{[]int{-7, -5, -3, 0, 1, 2}, 2}, 3},
		{"Sample 14", args{[]int{-7, -5, -3, 0, 1, 2}, 1}, 0},
		{"Sample 15", args{[]int{-7, -5, -3, 0, 1, 2}, -7}, -7},
		{"Sample 16", args{[]int{-7, -5, -3, 0, 1, 2}, -8}, -8},
		{"Sample 17", args{[]int{-7, -5, -3, 0, 1, 2}, -9}, -9},
		{"Sample 18", args{[]int{-7, -5, -3, 0, 1, 2}, -10}, -10},
		{"Sample 19", args{[]int{-7, -5, -3, 0, 1, 2}, -11}, -11},
		{"Sample 20", args{[]int{-7, -5, -3, 0, 1, 2}, -12}, -12},
		{"Sample 21", args{[]int{-7, -5, -3, 0, 1, 2}, -13}, -12},
		{"Sample 22", args{[]int{-7, -5, -3, 0, 1, 2}, 0}, 0},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := threeSumClosest(tt.args.nums, tt.args.target); got != tt.want {
				t.Errorf("threeSumClosest() = %v, want %v", got, tt.want)
			}
		})
	}
}
