package problem_16

import (
	"math"
	"sort"
)

func Abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func threeSumClosest(nums []int, target int) int {
	sort.Ints(nums)

	l := len(nums)

	if target < -3000 {
		return nums[0] + nums[1] + nums[2]
	}

	if target > 3000 {
		return nums[l-3] + nums[l-2] + nums[l-1]
	}

	closest := math.MaxInt32
	closestAbs := math.MaxInt32

	for i := l - 2; i >= 0; i-- {
		currentDiff := nums[i] - target

		left, right := i+1, l-1

		for left < right {
			sum := currentDiff + nums[left] + nums[right]

			if Abs(sum) < closestAbs {
				closest = sum
				closestAbs = Abs(closest)
			}

			if sum > 0 {
				right--
			} else {
				left++
			}
		}
	}

	return closest + target
}
