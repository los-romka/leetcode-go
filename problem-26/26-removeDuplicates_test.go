package problem_26

import (
	"reflect"
	"testing"
)

func Test_removeDuplicates(t *testing.T) {
	type args struct {
		nums []int
	}
	tests := []struct {
		name         string
		args         args
		want         int
		expectedNums []int
	}{
		{"Example 1", args{[]int{1, 1, 2}}, 2, []int{1, 2}},
		{"Example 2", args{[]int{0, 0, 1, 1, 1, 2, 2, 3, 3, 4}}, 5, []int{0, 1, 2, 3, 4}},
		{"Sample 1", args{[]int{0, 0}}, 1, []int{0}},
		{"Sample 2", args{[]int{0, 1, 1}}, 2, []int{0, 1}},
		{"Sample 3", args{[]int{0, 1, 2}}, 3, []int{0, 1, 2}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			k := removeDuplicates(tt.args.nums)

			if k != tt.want {
				t.Errorf("removeDuplicates() = %v, want %v", k, tt.want)
			}

			if !reflect.DeepEqual(tt.args.nums[:k], tt.expectedNums) {
				t.Errorf("removeDuplicates() = %v, want %v", tt.args.nums[:k], tt.expectedNums)
			}
		})
	}
}
