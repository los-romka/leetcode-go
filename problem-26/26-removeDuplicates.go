package problem_26

func removeDuplicates(nums []int) int {
	l := len(nums) - 1

	if l == 0 {
		return 1
	}

	i := 0

	for j := 0; j <= l; j++ {
		if j < l && nums[j] == nums[j+1] {
			continue
		}

		nums[i] = nums[j]

		i++
	}

	return i
}
