package problem_10

func isMatch(s string, p string) bool {
	matrix := make([][]bool, len(s)+1)

	for i := 0; i <= len(s); i++ {
		matrix[i] = make([]bool, len(p)+1)
	}

	matrix[0][0] = true

	for j := 1; j <= len(p); j++ {
		if p[j-1] == '*' {
			matrix[0][j] = matrix[0][j-2]
		}
	}

	for i := 0; i < len(s); i++ {
		for j := 0; j < len(p); j++ {
			if matrix[i][j] && (p[j] == s[i] || p[j] == '.') {
				matrix[i+1][j+1] = true
			} else if p[j] == '*' {
				if matrix[i][j] && (p[j-1] == s[i] || p[j-1] == '.') {
					matrix[i+1][j+1] = true
				} else if matrix[i+1][j-1] {
					matrix[i+1][j+1] = true
				} else if matrix[i][j+1] && matrix[i+1][j] {
					matrix[i+1][j+1] = true
				} else if matrix[i][j+1] && (p[j-1] == s[i] || p[j-1] == '.') {
					matrix[i+1][j+1] = true
				}
			}
		}
	}

	return matrix[len(s)][len(p)]
}
