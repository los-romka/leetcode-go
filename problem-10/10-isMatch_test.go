package problem_10

import "testing"

func Test_isMatch(t *testing.T) {
	type args struct {
		s string
		p string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{"Example 1", args{"aa", "a"}, false},
		{"Example 2", args{"aa", "a*"}, true},
		{"Example 3", args{"ab", ".*"}, true},
		{"Test 1", args{"aaa", "a*a"}, true},
		{"Test 2", args{"aaa", "ab*a*c*a"}, true},
		{"Test 3", args{"ccbbabbbabababa", ".*.ba*c*c*aab.a*b*"}, false},
		{"Test 4", args{"aaaaaaaaaaaaaaaaaaab", "a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*"}, false},
		{"Sample 1", args{"aa", "aa"}, true},
		{"Sample 2", args{"aa", ".."}, true},
		{"Sample 3", args{"aa", ".a"}, true},
		{"Sample 4", args{"aa", "a."}, true},
		{"Sample 5", args{"ab", "aba"}, false},
		{"Sample 6", args{"db", "ab"}, false},
		{"Sample 7", args{"aaab", "a*ab"}, true},
		{"Sample 8", args{"aaba", "a*aba"}, true},
		{"Sample 9", args{"", "a*"}, true},
		{"Sample 10", args{"aa", "aa*a"}, true},
		{"Sample 11", args{"aaa", "aa*a"}, true},
		{"Sample 12", args{"aaaaaaaaaaaaaaaaaaab", "a*a*b*"}, true},
		{"Sample 13", args{"abc", "a*b*c*abca*b*c*"}, true},
		{"Sample 14", args{"abcd", "abc.*d"}, true},
		{"Sample 15", args{"abcd", "abc.*e"}, false},
		{"Sample 16", args{"abcd", "abc.*e*"}, true},
		{"Sample 17", args{"abcdef", "ab.*de.*"}, true},
		{"Sample 18", args{"aaaaaaaaaaaaaaaaa", "a.*a.*a.*a.*a.*a.*a.*a.*a"}, true},
		{"Sample 19", args{"ab", ".*c*b"}, true},
		{"Sample 20", args{"aa", "aa"}, true},
		{"Sample 21", args{"ba", ".*a"}, true},
		{"Sample 22", args{"abc", "a*b*c*abca*b*c*"}, true},
		{"Sample 23", args{"abc", "abc"}, true},
		{"Sample 24", args{"a", ".*"}, true},
		{"Sample 25", args{"ab", ".*"}, true},
		{"Sample 26", args{"abc", ".*"}, true},
		{"Sample 27", args{"abcd", ".*"}, true},
		{"Sample 28", args{"abcde", ".*"}, true},
		{"Sample 29", args{"ba", ".*.*a"}, true},
		{"Sample 30", args{"aa", "aa*"}, true},
		{"Sample 31", args{"b", "a*a*"}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isMatch(tt.args.s, tt.args.p); got != tt.want {
				t.Errorf("isMatch() = %v, want %v", got, tt.want)
			}
		})
	}
}
