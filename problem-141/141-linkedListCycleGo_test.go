package problem_141

import (
	"testing"
)

func Test_removeDuplicates(t *testing.T) {
	type args struct {
		head *ListNode
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{"Sample 1", args{makeLinkedList([]int{3, 2, 0, -4}, 1)}, true},
		{"Sample 2", args{makeLinkedList([]int{1, 2}, 0)}, true},
		{"Sample 3", args{makeLinkedList([]int{1, 2}, -1)}, false},
		{"Test 1", args{makeLinkedList([]int{}, -1)}, false},
		{"Test 2", args{makeLinkedList([]int{1}, 0)}, true},
		{"Test 3", args{makeLinkedList([]int{-21, 10, 17, 8, 4, 26, 5, 35, 33, -7, -16, 27, -12, 6, 29, -12, 5, 9, 20, 14, 14, 2, 13, -24, 21, 23, -21, 5}, -1)}, false},
		{"Example 1", args{makeLinkedList([]int{1, 2}, 1)}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			k := hasCycle(tt.args.head)

			if k != tt.want {
				t.Errorf("hasCycle() = %v, want %v", k, tt.want)
			}
		})
	}
}

func makeLinkedList(list []int, pos int) *ListNode {
	if len(list) == 0 {
		return nil
	}

	var head, tail, loop *ListNode

	for i, v := range list {
		current := &ListNode{Val: v}

		if pos == i {
			loop = current
		}

		if head == nil {
			head = current
		}

		if tail != nil {
			tail.Next = current
		}

		tail = current
	}

	tail.Next = loop

	return head
}
