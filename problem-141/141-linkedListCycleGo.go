package problem_141

type ListNode struct {
	Val  int
	Next *ListNode
}

func hasCycle(head *ListNode) bool {
	var power, lam int64 = 1, 1

	if head == nil {
		return false
	}

	tortoise := head

	hare := tortoise.Next

	for tortoise != hare && hare != nil {
		if power == lam {
			tortoise = hare
			power *= 2
			lam = 0
		}

		hare = hare.Next
		lam++
	}

	tortoise, hare = head, head

	mu := int64(0) // i
	for ; mu < lam; mu++ {
		hare = hare.Next
	}

	mu = 0
	for tortoise != hare && tortoise != nil && hare != nil {
		tortoise = tortoise.Next
		hare = hare.Next

		mu++
	}

	return lam >= 0 && tortoise == hare
}
