package problem_11

import "testing"

func Test_maxArea(t *testing.T) {
	type args struct {
		height []int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"Example 1", args{[]int{1, 8, 6, 2, 5, 4, 8, 3, 7}}, 49},
		{"Example 2", args{[]int{1, 1}}, 1},
		{"Test 1", args{[]int{1, 3, 2, 5, 25, 24, 5}}, 24},
		{"Sample 1", args{[]int{1, 3, 2}}, 2},
		{"Sample 2", args{[]int{3, 1, 2}}, 4},
		{"Sample 3", args{[]int{1, 2, 3}}, 2},
		{"Sample 4", args{[]int{5, 20, 20, 5}}, 20},
		{"Sample 5", args{[]int{40, 40, 20, 5}}, 40},
		{"Sample 6", args{[]int{40, 40, 40, 5}}, 80},
		{"Sample 7", args{[]int{1, 3, 2, 5, 25, 25, 5}}, 25},
		{"Sample 8", args{[]int{1, 3, 2, 5, 25, 40, 5, 25}}, 75},
		{"Sample 9", args{[]int{1, 3, 2, 5, 25, 40, 5, 40}}, 80},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := maxArea(tt.args.height); got != tt.want {
				t.Errorf("maxArea() = %v, want %v", got, tt.want)
			}
		})
	}
}
