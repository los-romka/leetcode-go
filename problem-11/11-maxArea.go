package problem_11

func pourWater(l int, r int, height []int) int {
	if l < 0 {
		l = 0
	}

	if r >= len(height) {
		r = len(height) - 1
	}

	if height[l] < height[r] {
		return (r - l) * height[l]
	} else {
		return (r - l) * height[r]
	}
}

func maxArea(height []int) int {
	l := 0
	r := len(height) - 1

	max := pourWater(l, r, height)

	for l < r {
		if height[l] < height[r] {
			l++
		} else {
			r--
		}

		h := pourWater(l, r, height)

		if max < h {
			max = h
		}
	}

	return max
}
