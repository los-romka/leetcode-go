package problem_14

import "testing"

func Test_longestCommonPrefix(t *testing.T) {
	type args struct {
		strs []string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"Example 1", args{[]string{"flower", "flow", "flight"}}, "fl"},
		{"Example 2", args{[]string{"dog", "racecar", "car"}}, ""},
		{"Test 1", args{[]string{"", ""}}, ""},
		{"Test 2", args{[]string{"flower", "flower", "flower", "flower"}}, "flower"},
		{"Sample 1", args{[]string{"flower", "flow"}}, "flow"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := longestCommonPrefix(tt.args.strs); got != tt.want {
				t.Errorf("longestCommonPrefix() = %v, want %v", got, tt.want)
			}
		})
	}
}
