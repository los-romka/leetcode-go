package problem_14

func longestCommonPrefix(strs []string) string {
	if len(strs) == 1 {
		return strs[0]
	}

	source := strs[0]
	j := 0

	for ; j < len(strs[0]); j++ {
		char := strs[0][j]

		for i := 1; i < len(strs); i++ {
			if j >= len(strs[i]) || char != strs[i][j] {
				return source[0:j]
			}
		}
	}

	return source[0:j]
}
