package problem_6

func convert(s string, numRows int) string {
	if numRows == 1 {
		return s
	}

	slen := len(s)
	result := make([]byte, slen)
	jump := (numRows - 1) * 2

	jump1 := 0
	jump2 := 0

	i := 0
	cur := 0

	for row := 0; row < numRows; row++ {
		if row == 0 || row == numRows-1 {
			for i = row; i < slen; i = i + jump {
				result[cur] = s[i]
				cur++
			}
		} else {
			jump1 = row * 2
			jump2 = jump - jump1

			for i = row; i < slen; i = i + jump1 {
				result[cur] = s[i]

				jump2, jump1 = jump1, jump2
				cur++
			}
		}
	}

	return string(result)
}
